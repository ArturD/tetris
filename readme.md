**Tetris**

Python: 3.8

Used:
black, flake8, mypy, isort

**Running:**

`pip install -r requirements.txt`

`python main.py`

**Tests**

`tox .`
