import builtins
import unittest

import mock
from source.game_engine import GameEngine
from source.input_actions import InputAction
from parameterized import parameterized  # ignore: ignore
from source.piece import Piece


class TestGameEngine(unittest.TestCase):
    def test_create_board(self) -> None:
        engine = GameEngine()
        self.assertTrue(engine.board is not None)

    def test_create_available_pieces(self) -> None:
        engine = GameEngine()
        self.assertTrue(len(engine.available_pieces) > 0)

    @parameterized.expand(
        [
            [InputAction.ACTION_MOVE_LEFT, -1, 0],
            [InputAction.ACTION_MOVE_RIGHT, 1, 0],
            [InputAction.ACTION_ROTATE, 0, 1],
            [InputAction.ACTION_ROTATE_COUNTER, 0, -1],
        ]
    )
    def test_user_input_actions(
        self, action: InputAction, expected_move_x: int, expected_rotate: int
    ) -> None:
        engine = GameEngine()
        with mock.patch.object(builtins, "input", lambda _: action.value):
            self.assertEqual(
                engine.get_user_action(), (expected_move_x, expected_rotate)
            )

    def test_generate_new_piece(self) -> None:
        engine = GameEngine()
        self.assertIsInstance(engine.generate_new_piece(), Piece)
