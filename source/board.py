import copy
from random import randint
from typing import List, Optional

from source.piece import Piece


class Board:
    BOUND_SIGN = "*"
    piece_in_move: Optional[Piece] = None
    board: List[List[str]]

    def __init__(self, width: int = 20, height: int = 20):
        self.width: int = width
        self.height: int = height
        self.board = [[" " for row in range(width)] for column in range(height)]
        self._init_board_bounds()

    def _update_board(self) -> None:
        if not self.piece_in_move:
            return

        indexes = self.piece_in_move.get_body_indexes()

        touched_x_bounds = any(
            [
                self.board[self.piece_in_move.y + y + 1][self.piece_in_move.x + x]
                == self.BOUND_SIGN
                for x, y in indexes
            ]
        )

        if touched_x_bounds:
            for x, y in indexes:
                self.board[self.piece_in_move.y + y][
                    self.piece_in_move.x + x
                ] = self.BOUND_SIGN
            self.piece_in_move = None

    def _init_board_bounds(self) -> None:
        for column in range(self.height - 1):
            self.board[column][0] = self.board[column][-1] = self.BOUND_SIGN
        self.board[-1] = [self.BOUND_SIGN for x in range(self.width)]

    def show_board(self) -> None:
        indexes = []
        if self.piece_in_move:
            indexes = self.piece_in_move.get_body_indexes()

        board_to_display = copy.deepcopy(self.board)
        if self.piece_in_move:
            for x, y in indexes:
                board_to_display[self.piece_in_move.y + y][
                    self.piece_in_move.x + x
                ] = self.piece_in_move.BODY_SIGN

        for column_index, column in enumerate(board_to_display):
            print(" ".join(column))

    def add_piece(self, piece: Piece) -> None:
        piece.x = randint(1, self.width - 2)  # We must also count bounds that's why -2
        piece.y = 0
        self.piece_in_move = piece
        self._update_board()

    def can_move(self, move_x: int = 0, rotate: int = 0) -> bool:
        check_piece = copy.deepcopy(self.piece_in_move)
        if not check_piece:
            return False

        indexes = check_piece.get_body_indexes()
        if rotate == 1:
            check_piece.rotate()
        elif rotate == -1:
            check_piece.rotate_counter_clockwise()

        is_next_obstacle = any(
            [
                self.board[check_piece.y + y + 1][check_piece.x + move_x]
                == self.BOUND_SIGN
                for x, y in indexes
            ]
        )
        max_x, max_y = check_piece.get_max_indexes()
        in_limit = (max_y + 1 < self.height) and max_x < self.width
        can_move = bool(not is_next_obstacle and in_limit and check_piece)
        return can_move

    def move_piece(self, move_x: int, rotate: int) -> bool:
        if not self.can_move(move_x, rotate) or not self.piece_in_move:
            return False

        if rotate == 1:
            self.piece_in_move.rotate()
        elif rotate == -1:
            self.piece_in_move.rotate_counter_clockwise()

        self.piece_in_move.y += 1
        self.piece_in_move.x += move_x
        self._update_board()
        return True
