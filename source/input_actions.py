from enum import Enum


class InputAction(Enum):
    ACTION_MOVE_LEFT = "a"
    ACTION_MOVE_RIGHT = "d"
    ACTION_ROTATE = "w"
    ACTION_ROTATE_COUNTER = "s"
    ACTION_EXIT = "e"
    ACTION_NOTHING = ""

    @classmethod
    def has_value(cls, value: str) -> bool:
        return value in cls._value2member_map_  # type: ignore
