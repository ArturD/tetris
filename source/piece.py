from itertools import zip_longest
from typing import List, Tuple


class Piece:

    x = 0
    y = 0
    BODY_SIGN = "0"
    piece_body: List[List[str]]

    def __init__(self, raw_body: str):
        self.create_from_str(raw_body)

    def create_from_str(self, raw_body: str) -> None:
        splited_raw_body = [line for line in raw_body.split("\n") if line.strip() != ""]
        self.piece_body = [list(line) for line in splited_raw_body]

    def show_piece(self) -> None:
        for column in self.piece_body:
            print(" ".join(column))

    def get_body_indexes(self) -> List[List[int]]:
        indexes = []
        for y, column in enumerate(self.piece_body):
            x_indexes = [i for i, x in enumerate(column) if x == self.BODY_SIGN]
            indexes.extend([[x, y] for x in x_indexes])
        return indexes

    def get_max_indexes(self) -> Tuple[int, int]:
        return self.x + len(self.piece_body[0]), self.y + len(self.piece_body)

    def rotate(self) -> None:
        self.piece_body = list(map(list, zip_longest(*self.piece_body, fillvalue="")))

    def rotate_counter_clockwise(self) -> None:
        self.piece_body = list(
            map(list, zip_longest(*reversed(self.piece_body), fillvalue=""))
        )
