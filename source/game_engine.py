import copy
import random
from typing import List, Tuple

from source.board import Board
from source.input_actions import InputAction
from source.piece import Piece
from settings import (tetris_piece_cube, tetris_piece_L, tetris_piece_line,
                      tetris_piece_reversed_L, tetris_piece_reversed_snake)


class GameEngine:
    __in_game: bool = False
    board: Board
    available_pieces: List[Piece]

    def __init__(self, board_width: int = 20, board_height: int = 20):
        self.board = Board(board_width, board_height)
        self.available_pieces = [
            Piece(tetris_piece_line),
            Piece(tetris_piece_L),
            Piece(tetris_piece_reversed_L),
            Piece(tetris_piece_reversed_snake),
            Piece(tetris_piece_cube),
        ]

    def start(self) -> None:
        self.__in_game = True
        while self.__in_game:
            self.next_round()

    def get_user_action(self) -> Tuple[int, int]:
        command = input("")
        move_x = 0
        rotate = 0

        if not InputAction.has_value(command):
            print("Wrong action: Pick one of a,d,w,s (e-exit)")
            return move_x, rotate
        if command == InputAction.ACTION_MOVE_LEFT.value:
            move_x = -1
        elif command == InputAction.ACTION_MOVE_RIGHT.value:
            move_x = 1
        elif command == InputAction.ACTION_ROTATE.value:
            rotate = 1
        elif command == InputAction.ACTION_ROTATE_COUNTER.value:
            rotate = -1

        if command == "e":
            self.__in_game = False

        return move_x, rotate

    def next_round(self) -> None:
        if not self.board.piece_in_move:
            self.board.add_piece(self.generate_new_piece())
            if not self.board.can_move():
                print("END OF GAME")
                self.board.show_board()
                self.__in_game = False
                return

        self.board.show_board()
        move_x, rotate = self.get_user_action()
        valid_move = self.board.move_piece(move_x, rotate)
        if not valid_move:
            print("Invalid move, choose something different")

    def generate_new_piece(self) -> Piece:
        piece = copy.deepcopy(random.choice(self.available_pieces))
        return piece
